'use strict';

let Client = require('ssh2-sftp-client');
let fs = require('fs');
let sftp = new Client();
 
function upload()
{
let remote = '/sftpuser/sftp-demo/test.txt';
let data = fs.createReadStream('/home/setgau01/poc/nodesftppoc/index.js');

sftp.connect({
host: "127.0.0.1",
username: "sftpuser",
password: "sftppassword",
port: 22
    }).then(() => {
     return sftp.put(data, remote);
//  return sftp.list('/sftpuser/sftp-demo');
}).then(data => {
  console.log(data, 'the data info');
}).catch(err => {
  console.log(err, 'catch error');
});
}

function download()
{

let remote = '/sftpuser/sftp-demo/test.txt';
let local = '/home/setgau01/poc/nodesftppoc/test.js';

sftp.connect({
host: "127.0.0.1",
username: "sftpuser",
password: "sftppassword",
port: 22
    }).then(() => {
     return sftp.fastGet(remote, local);
//  return sftp.list('/sftpuser/sftp-demo');
}).then(data => {
  console.log(data, 'the data info');
}).catch(err => {
  console.log(err, 'catch error');
});
}

download();


