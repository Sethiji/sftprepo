SFTP Setup in local ubuntu System
https://linuxconfig.org/how-to-setup-sftp-server-on-ubuntu-18-04-bionic-beaver-with-vsftpd


Useful link for SFTP
https://www.npmjs.com/package/ssh2-sftp-client#orgbbb7529
https://www.npmjs.com/package/sftp-upload

Connect with SFTP
let Client = require('ssh2-sftp-client');
let sftp = new Client();

sftp.connect({
host: "127.0.0.1",
username: "sftpuser",
password: "sftppassword",
port: 22
    }).then(() => {
return sftp.list('/sftpuser/sftp-demo');
});

Upload file
let Client = require('ssh2-sftp-client');
let fs = require('fs');
let sftp = new Client();
 
function upload()
{
let remote = '/sftpuser/sftp-demo/test.txt';
let data = fs.createReadStream('/home/setgau01/poc/nodesftppoc/index.js');

sftp.connect({
host: "127.0.0.1",
username: "sftpuser",
password: "sftppassword",
port: 22
    }).then(() => {
     return sftp.put(data, remote);
//  return sftp.list('/sftpuser/sftp-demo');
}).then(data => {
  console.log(data, 'the data info');
}).catch(err => {
  console.log(err, 'catch error');
});
}


Download file
let Client = require('ssh2-sftp-client');
let fs = require('fs');
let sftp = new Client();

function download()
{

let remote = '/sftpuser/sftp-demo/test.txt';
let local = '/home/setgau01/poc/nodesftppoc/test.js';

sftp.connect({
host: "127.0.0.1",
username: "sftpuser",
password: "sftppassword",
port: 22
    }).then(() => {
     return sftp.fastGet(remote, local);
//  return sftp.list('/sftpuser/sftp-demo');
}).then(data => {
  console.log(data, 'the data info');
}).catch(err => {
  console.log(err, 'catch error');
});
}


